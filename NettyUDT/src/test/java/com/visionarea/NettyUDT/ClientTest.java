package com.visionarea.NettyUDT;

import com.barchart.udt.MonitorUDT;
import com.barchart.udt.OptionUDT;
import com.barchart.udt.SocketUDT;
import com.barchart.udt.TypeUDT;
import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.net.InetSocketAddress;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class ClientTest {

	private static boolean run = true;
	private static Logger log = LoggerFactory.getLogger(ClientTest.class);
	private static SocketUDT clientSocket;
	private static InetSocketAddress remoteSocketAddress;
	private static final int DATA_SIZE = 1024;
	private static boolean receive;

	/**
	 * @param args
	 *            the command line arguments
	 * @throws IOException
	 */
	public static void main(String[] args) throws IOException {

		log.info("started CLIENT");

		// specify client sendSocket interface
		final String bindAddress = "localhost";

		// specify server listening address
		final String remoteAddress = "localhost";

		// specify server listening port
		final int remotePort = 1337;

		// specify server bandwidth limit
		final long maxBandwidth = 1000000000;

		// specify number of packet batches between stats logging
		final int countMonitor = 100;

		try {
			clientSocket = new SocketUDT(TypeUDT.DATAGRAM);
			log.info("init; sendSocket={}", clientSocket.getSocketId());

			// specify maximum upload speed, bytes/sec
			clientSocket.setOption(OptionUDT.UDT_MAXBW, maxBandwidth);

			InetSocketAddress localSocketAddress = new InetSocketAddress(
					bindAddress, 0);

			log.info("localSocketAddress : {}", localSocketAddress);

			clientSocket.bind(localSocketAddress);
			localSocketAddress = clientSocket.getLocalSocketAddress();
			log.info("sendSocket bind; localSocketAddress={}",
					localSocketAddress);

			remoteSocketAddress = new InetSocketAddress(remoteAddress,
					remotePort);

			// specify maximum upload speed, bytes/sec
			clientSocket.setOption(OptionUDT.UDT_MAXBW, maxBandwidth);

			// specify blocking receive
			clientSocket.setOption(OptionUDT.UDT_RCVSYN, true);

			// specify receive timeout of 2 seconds
			clientSocket.setOption(OptionUDT.UDT_RCVTIMEO, 2000);

			clientSocket.connect(remoteSocketAddress);
			remoteSocketAddress = clientSocket.getRemoteSocketAddress();
			log.info("connect; remoteSocketAddress={}", remoteSocketAddress);

			StringBuilder text = new StringBuilder(1024);
			OptionUDT.appendSnapshot(clientSocket, text);
			text.append("\t\n");
			log.info("clientSocket options; {}", text);

			long count = 0;

			final MonitorUDT monitor = clientSocket.getMonitor();

			receive = false;

			while (run) {

				if (receive) {

					byte[] receiveData = new byte[DATA_SIZE];

					int receiveResult = clientSocket.receive(receiveData);

					if (receiveResult > 0) {
						String message = new String(receiveData).trim();

						log.info("received {} bytes", receiveResult);
						
						log.info("RESPONSE MSG [{}]", message);
						
						if (message.equals("bye")) {
							clientSocket.close();
							run = false;
						}
						receive = false;
						continue;
					}
				} else {

					String request = readMessageFromInput();

					if (request == null) {
						continue;
					}

					byte[] requestData = request.getBytes();

					final int sendResult = clientSocket.send(requestData);

					assert sendResult == requestData.length : "wrong size";

					log.info("sent {} bytes", sendResult);

					receive = true;
				}
				count++;

				if (count % countMonitor == 0) {
					clientSocket.updateMonitor(false);
					text = new StringBuilder(1024);
					monitor.appendSnapshot(text);
					log.info("stats; {}", text);
				}
			}
			clientSocket.close();

		} catch (Throwable e) {
			log.error("unexpected", e);
		} finally {
			clientSocket.close();
			System.exit(-1);
		}
	}

	private static String readMessageFromInput() throws IOException {

		BufferedReader in = new BufferedReader(new InputStreamReader(System.in));

		System.out.print(">");

		String input = in.readLine();

		if (input.length() == 0) {
			return null;
		} else if (input.equals("D")) {
			clientSocket.close();
			run = false;
			return null;
		}

		return input;
	}
}
