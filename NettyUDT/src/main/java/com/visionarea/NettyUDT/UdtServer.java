package com.visionarea.NettyUDT;

import java.net.InetSocketAddress;
import java.util.concurrent.Executors;

import org.jboss.netty.bootstrap.ServerBootstrap;

import com.barchart.udt.nio.SelectorProviderUDT;
import com.visionarea.netty.udt.nio.NioUdtServerSocketChannelFactory;

public class UdtServer {

	final int NUM_IO_WORKERS = 1;
	
	// server listening interface
	final String bindAddress;

	// server listening port
	final int localPort;

	public UdtServer(String bindAddress, int localPort) {
		this.bindAddress = bindAddress;
		this.localPort = localPort;
	}

	public void run() {
		// Configure the server.
		ServerBootstrap bootstrap = new ServerBootstrap(
				new NioUdtServerSocketChannelFactory(
						Executors.newCachedThreadPool(),
						Executors.newCachedThreadPool(),
						NUM_IO_WORKERS, 
						SelectorProviderUDT.DATAGRAM));

		// Configure the pipeline factory.
		bootstrap.setPipelineFactory(new UdtServerPipelineFactory());

		// Bind and start to accept incoming connections.
		bootstrap.bind(new InetSocketAddress(bindAddress, localPort));

		System.out.println("UDTServer running on "
				+ new InetSocketAddress("localhost", localPort));
	}

	public static void main(String[] args) throws Exception {

		new UdtServer("localhost", 1337).run();
	}
}
