/*
 * Copyright 2012 The Netty Project
 *
 * The Netty Project licenses this file to you under the Apache License,
 * version 2.0 (the "License"); you may not use this file except in compliance
 * with the License. You may obtain a copy of the License at:
 *
 *   http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS, WITHOUT
 * WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. See the
 * License for the specific language governing permissions and limitations
 * under the License.
 */
package com.visionarea.netty.udt.nio;

import java.util.concurrent.Executor;
import java.util.concurrent.Executors;

import org.jboss.netty.channel.ChannelPipeline;
import org.jboss.netty.channel.ChannelSink;
import org.jboss.netty.channel.socket.ServerSocketChannel;
import org.jboss.netty.channel.socket.ServerSocketChannelFactory;
import org.jboss.netty.channel.socket.nio.NioWorker;
import org.jboss.netty.channel.socket.nio.WorkerPool;
import org.jboss.netty.util.ExternalResourceReleasable;
import org.jboss.netty.util.internal.ExecutorUtil;

import com.barchart.udt.nio.SelectorProviderUDT;


public class NioUdtServerSocketChannelFactory implements ServerSocketChannelFactory{
	
	final Executor bossExecutor;
    private final WorkerPool<NioUdtWorker> workerPool;
    private final ChannelSink sink;
    
    private final SelectorProviderUDT providerUDT;
    
    /**
     * Create a new {@link NioUdtServerSocketChannelFactory} using {@link Executors#newCachedThreadPool()}
     * for the boss and worker.
     *
     * See {@link #UDTNioServerSocketChannelFactory(Executor, Executor)}
     */
    public NioUdtServerSocketChannelFactory() {
        this(Executors.newCachedThreadPool(), Executors.newCachedThreadPool());
    }

    /**
     * Creates a new instance.  Calling this constructor is same with calling
     * {@link #UDTNioServerSocketChannelFactory(Executor, Executor, int)} with 2 *
     * the number of available processors in the machine.  The number of
     * available processors is obtained by {@link Runtime#availableProcessors()}.
     *
     * @param bossExecutor
     *        the {@link Executor} which will execute the boss threads
     * @param workerExecutor
     *        the {@link Executor} which will execute the I/O worker threads
     */
    public NioUdtServerSocketChannelFactory(
            Executor bossExecutor, Executor workerExecutor) {
        this(bossExecutor, workerExecutor, UdtSelectorUtil.DEFAULT_IO_THREADS, UdtSelectorUtil.DEFAULT_SELECTOR_PROVIDER);
    }

    /**
     * Creates a new instance.
     *
     * @param bossExecutor
     *        the {@link Executor} which will execute the boss threads
     * @param workerExecutor
     *        the {@link Executor} which will execute the I/O worker threads
     * @param workerCount
     *        the maximum number of I/O worker threads
     */
    public NioUdtServerSocketChannelFactory(
            Executor bossExecutor, Executor workerExecutor,
            int workerCount, SelectorProviderUDT providerUDT) {
        this(bossExecutor, new NioUdtWorkerPool(workerExecutor, workerCount, providerUDT), providerUDT);
    }

    /**
     * Creates a new instance.
     *
     * @param bossExecutor
     *        the {@link Executor} which will execute the boss threads
     * @param workerPool
     *        the {@link WorkerPool} which will be used to obtain the {@link NioWorker} that execute
     *        the I/O worker threads
     */
    public NioUdtServerSocketChannelFactory(
            Executor bossExecutor, WorkerPool<NioUdtWorker> workerPool, SelectorProviderUDT providerUDT) {
        if (bossExecutor == null) {
            throw new NullPointerException("bossExecutor");
        }
        if (workerPool == null) {
            throw new NullPointerException("workerPool");
        }
        if (providerUDT == null) {
            throw new NullPointerException("providerUDT");
        }

        this.bossExecutor = bossExecutor;
        this.workerPool = workerPool;
        this.providerUDT = providerUDT;
        
        sink = new NioUdtServerSocketPipelineSink(workerPool, providerUDT);
    }

    public ServerSocketChannel newChannel(ChannelPipeline pipeline) {
        return new NioUdtServerSocketChannel(this, pipeline, sink, providerUDT);
    }

    public void releaseExternalResources() {
        ExecutorUtil.terminate(bossExecutor);
        if (workerPool instanceof ExternalResourceReleasable) {
            ((ExternalResourceReleasable) workerPool).releaseExternalResources();
        }
    }
}
